# Contributing to NHS Number Library

## A quick guide on how to contribute

## Contribution from Team

1. Fork the project

2. Clone the repo from your own space:
   
        git clone git@gitlab.com:nhsbsa/Libraries/bsa-time-jdk-7.git

3. Use [README.md](/README.md) instructions to build/run.

4. Run the tests. Merge requests that add features must include unit tests,
   so it is good to ensure you've got them passing to begin with.
     
5. Add your functionality or bug fix and a test for your change. Only refactoring and
   documentation changes do not require tests. If the functionality is at all complicated
   then it is likely that more than one test will be required. If you would like help
   with writing tests please do ask us.

5. Make sure all the tests pass.

6. Push to your fork, and submit a merge request.
## Guidelines for making a merge request

    The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
    "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be
    interpreted as described in RFC 2119.

## In order for a merge request to be accepted, it MUST

- Include at least one test (unless it is documentation or refactoring). If you have any questions about how to write tests, please ask us, we will be happy to help
- Include a clear summary in the merge request comments as to what the change is and why
  you are making it
- Be readable - we might ask you to change unclear variable names or obscure syntactic sugar
- Have [good commit messages](http://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message)
  that explain the change being made in that commit. Don't be afraid to write a lot in the
  detail.

## Contribution beyond the team

you have an idea to share or a feature to request to raise please contact the NHSBSA team.

Email : nhsbsa.digiadmin@nhs.net

## How soon will we respond?

We will comment on your issue as soon as we can. If you feel your issue is important, please feel free to bump it by making a comment on it.


