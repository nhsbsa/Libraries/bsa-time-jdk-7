package uk.nhs.nhsbsa.time.compositedate;

import org.junit.Test;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import uk.nhs.nhsbsa.time.compositedate.CompositeDate;

public class CompositeDatePojoTest {

    @Test
    public void testPojo() {
        Validator validator = ValidatorBuilder.create()
                .with(new GetterMustExistRule())
                .with(new SetterMustExistRule())
                .build();
        validator.validate(PojoClassFactory.getPojoClass(CompositeDate.class));
    }

    @Test
    public void testEqualsHashcode() {
        EqualsVerifier.forClass(CompositeDate.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }
}
