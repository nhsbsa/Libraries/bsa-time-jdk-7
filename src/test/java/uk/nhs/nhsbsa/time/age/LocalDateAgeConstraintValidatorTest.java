package uk.nhs.nhsbsa.time.age;

import static uk.nhs.nhsbsa.time.types.Comparison.EQ;
import org.joda.time.LocalDate;
import uk.nhs.nhsbsa.time.age.LocalDateAgeConstraintValidatorTest.AgeFixture;

public class LocalDateAgeConstraintValidatorTest
        extends AbstractDateAgeConstraintValidatorTest<AgeFixture> {

    @Override
    protected void setDate(AgeFixture fixture, LocalDate date) {
        fixture.setLocalDate(date);
    }

    @Override
    protected AgeFixture emptyFixture() {
        return new AgeFixture();
    }

    public class AgeFixture {

        @Age(comparison = EQ, threshold = "P1Y")
        private LocalDate localDate;

        public LocalDate getLocalDate() {
            return localDate;
        }

        public void setLocalDate(LocalDate age) {
            this.localDate = age;
        }
    }

}
