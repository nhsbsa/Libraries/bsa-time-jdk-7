package uk.nhs.nhsbsa.time.age;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.junit.Test;
import uk.nhs.nhsbsa.time.types.Comparison;

public class DateAgeValidatorTest {

    @Test
    public void nullIsValid() {
        assertTrue(execute(Comparison.EQ, Period.ZERO,
                null));
    }

    /* EQ */
    @Test
    public void todayIsValid_EQ_Zero() {
        assertTrue(execute(Comparison.EQ, Period.ZERO,
                LocalDate.now()));
    }

    @Test
    public void dayBeforeOneYearDateIsInvalid_EQ_OneYear() {
        assertFalse(execute(Comparison.EQ, Period.years(1),
                LocalDate.now().minusYears(1).plusDays(1)));
    }

    @Test
    public void oneYearDateIsValid_EQ_OneYear() {
        assertTrue(execute(Comparison.EQ, Period.years(1), LocalDate.now().minusYears(1)));
    }

    @Test
    public void dayAfterOneYearDateIsValid_EQ_OneYear() {
        assertTrue(execute(Comparison.EQ, Period.years(1),
                LocalDate.now().minusYears(1).minusDays(1)));
    }

    @Test
    public void dayBeforeTwoYearDateIsValid_EQ_OneYear() {
        assertTrue(execute(Comparison.EQ, Period.years(1),
                LocalDate.now().minusYears(2).plusDays(1)));
    }

    @Test
    public void twoYearDateIsInvalid_EQ_OneYear() {
        assertFalse(execute(Comparison.EQ, Period.years(1), LocalDate.now().minusYears(2)));
    }

    /* LT */
    @Test
    public void todayIsInvalid_LT_Zero() {
        assertFalse(execute(Comparison.LT, Period.ZERO,
                LocalDate.now()));
    }

    @Test
    public void dayBeforeOneYearDateIsValid_LT_OneYear() {
        assertTrue(execute(Comparison.LT, Period.years(1),
                LocalDate.now().minusYears(1).plusDays(1)));
    }

    @Test
    public void oneYearDateIsInvalid_LT_OneYear() {
        assertFalse(execute(Comparison.LT, Period.years(1), LocalDate.now().minusYears(1)));
    }

    @Test
    public void dayAfterOneYearDateIsInvalid_LT_OneYear() {
        assertFalse(execute(Comparison.LT, Period.years(1),
                LocalDate.now().minusYears(1).minusDays(1)));
    }

    @Test
    public void dayBeforeTwoYearDateIsInvalid_LT_OneYear() {
        assertFalse(execute(Comparison.LT, Period.years(1),
                LocalDate.now().minusYears(2).plusDays(1)));
    }

    @Test
    public void twoYearDateIsInvalid_LT_OneYear() {
        assertFalse(execute(Comparison.LT, Period.years(1), LocalDate.now().minusYears(2)));
    }

    /* LTE */
    @Test
    public void todayIsValid_LTE_Zero() {
        assertTrue(execute(Comparison.LTE, Period.ZERO,
                LocalDate.now()));
    }

    @Test
    public void dayBeforeOneYearDateIsValid_LTE_OneYear() {
        assertTrue(execute(Comparison.LTE, Period.years(1),
                LocalDate.now().minusYears(1).plusDays(1)));
    }

    @Test
    public void oneYearDateIsValid_LTE_OneYear() {
        assertTrue(execute(Comparison.LTE, Period.years(1), LocalDate.now().minusYears(1)));
    }

    @Test
    public void dayAfterOneYearDateIsValid_LTE_OneYear() {
        assertTrue(execute(Comparison.LTE, Period.years(1),
                LocalDate.now().minusYears(1).minusDays(1)));
    }

    @Test
    public void dayBeforeTwoYearDateIsValid_LTE_OneYear() {
        assertTrue(execute(Comparison.LTE, Period.years(1),
                LocalDate.now().minusYears(2).plusDays(1)));
    }

    @Test
    public void twoYearDateIsInvalid_LTE_OneYear() {
        assertFalse(execute(Comparison.LTE, Period.years(1), LocalDate.now().minusYears(2)));
    }

    /* GTE */
    @Test
    public void todayIsValid_GT_Zero() {
        assertTrue(execute(Comparison.GTE, Period.ZERO,
                LocalDate.now()));
    }

    @Test
    public void dayBeforeOneYearDateIsInvalid_GTE_OneYear() {
        assertFalse(execute(Comparison.GTE, Period.years(1),
                LocalDate.now().minusYears(1).plusDays(1)));
    }

    @Test
    public void oneYearDateIsValid_GTE_OneYear() {
        assertTrue(execute(Comparison.GTE, Period.years(1), LocalDate.now().minusYears(1)));
    }

    @Test
    public void dayAfterOneYearDateIsValid_GTE_OneYear() {
        assertTrue(execute(Comparison.GTE, Period.years(1),
                LocalDate.now().minusYears(1).minusDays(1)));
    }

    @Test
    public void dayBeforeTwoYearDateIsValid_GTE_OneYear() {
        assertTrue(execute(Comparison.GTE, Period.years(1),
                LocalDate.now().minusYears(2).plusDays(1)));
    }

    @Test
    public void twoYearDateIsInvalid_GTE_OneYear() {
        assertTrue(execute(Comparison.GTE, Period.years(1), LocalDate.now().minusYears(2)));
    }

    /* GT */
    @Test
    public void todayIsInvalid_GT_Zero() {
        assertFalse(execute(Comparison.GT, Period.ZERO,
                LocalDate.now()));
    }

    @Test
    public void dayBeforeOneYearDateIsInvalid_GT_OneYear() {
        assertFalse(execute(Comparison.GT, Period.years(1),
                LocalDate.now().minusYears(1).plusDays(1)));
    }

    @Test
    public void oneYearDateIsInvalid_GT_OneYear() {
        assertFalse(execute(Comparison.GT, Period.years(1), LocalDate.now().minusYears(1)));
    }

    @Test
    public void dayAfterOneYearDateIsInvalid_GT_OneYear() {
        assertFalse(execute(Comparison.GT, Period.years(1),
                LocalDate.now().minusYears(1).minusDays(1)));
    }

    @Test
    public void dayBeforeTwoYearDateIsInvalid_GT_OneYear() {
        assertFalse(execute(Comparison.GT, Period.years(1),
                LocalDate.now().minusYears(2).plusDays(1)));
    }

    @Test
    public void twoYearDateIsInvalid_GT_OneYear() {
        assertTrue(execute(Comparison.GT, Period.years(1), LocalDate.now().minusYears(2)));
    }

    private boolean execute(Comparison comparison, Period threshold, LocalDate date) {
        DateAgeValidator validator = new DateAgeValidator(comparison, threshold);
        return validator.isValid(date);
    }

}
