package uk.nhs.nhsbsa.time.age;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Min;
import uk.nhs.nhsbsa.time.types.Comparison;

@Documented
@Constraint(validatedBy = {
        LocalDateAgeConstraintValidator.class,
        CompositeDateAgeConstraintValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Age {

    String message() default "{AgeLimit.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    Comparison comparison();

    String threshold() default "P0D";

    /**
     * Defines several {@link Age} annotations on the same element.
     *
     * @see Min
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    @interface List {

        Age[] value();
    }
}

