package uk.nhs.nhsbsa.time.age;

import org.joda.time.DurationFieldType;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nhs.nhsbsa.time.types.Comparison;

class DateAgeValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateAgeValidator.class);
    
    /**
     * Support days, month, year granularity only.
     */
    private static final DurationFieldType[] units =
            new DurationFieldType[] {
                    DurationFieldType.days(), 
                    DurationFieldType.months(), 
                    DurationFieldType.years()};

    /**
     * Support standard EQ, LT, LTE, GTE, GT comparisons.
     */
    private Comparison comparison;

    /**
     * Age threshold defined as a Period.
     */
    private Period threshold;

    /**
     * Store granularity of threshold to allow date range comparisons in appropriate unit.
     */
    private DurationFieldType granularity;

    /**
     * Validator constructed with comparison and threshold.
     * 
     * @param comparison
     * @param threshold
     */
    public DateAgeValidator(Comparison comparison, Period threshold) {
        super();
        this.comparison = comparison;
        this.threshold = threshold;
        initialiseGranularity();
    }

    /**
     * Determine temporal granularity from threshold.
     */
    private void initialiseGranularity() {
        for (DurationFieldType temporalUnit : units) {
            if (threshold.get(temporalUnit) != 0) {
                granularity = temporalUnit;
                break;
            }
        }
        if (granularity == null) {
            granularity = DurationFieldType.days();
        }
    }

    /**
     * Compares date argument with threshold to determine age.
     */
    public boolean isValid(LocalDate date) {
        if (date != null) {
            LocalDate now = LocalDate.now();
            Integer ageLimit = age(now.minus(threshold), now);
            Integer age = age(date, now);
            boolean result = comparison.compare(age, ageLimit);
            LOGGER.info("{}: {} {} {} = {}", threshold, age, comparison, ageLimit, result);
            return result;
        }
        return true;
    }

    private Integer age(LocalDate date, LocalDate now) {
        return new Period(date, now).get(granularity);
    }

}
