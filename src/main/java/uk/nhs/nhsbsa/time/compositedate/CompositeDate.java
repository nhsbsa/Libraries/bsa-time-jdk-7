package uk.nhs.nhsbsa.time.compositedate;

import static uk.nhs.nhsbsa.time.util.NumberUtils.toInteger;
import static uk.nhs.nhsbsa.time.util.ObjectUtils.anyNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Three part date object for binding to three separate fields in UI.
 */
public class CompositeDate implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompositeDate.class);

    private String year;

    private String month;

    private String day;

    /**
     * Default constructor.
     */
    public CompositeDate() {
        super();
    }

    /**
     * Convenience constructor with Integer year, month, day.
     * 
     * @param year
     * @param month
     * @param day
     */
    public CompositeDate(Integer year, Integer month, Integer day) {
        super();
        this.year = Objects.toString(year, null);
        this.month = Objects.toString(month, null);
        this.day = Objects.toString(day, null);
    }

    /**
     * Convenience constructor with String year, month, day.
     * 
     * @param year
     * @param month
     * @param day
     */
    public CompositeDate(String year, String month, String day) {
        super();
        this.year = year;
        this.month = month;
        this.day = day;
    }

    /**
     * Convenience constructor with java.time.LocalDate.
     * 
     * @param localDate
     */
    public CompositeDate(LocalDate localDate) {
        this(localDate.getYear(), localDate.getMonthOfYear(), localDate.getDayOfMonth());
    }

    public LocalDate toLocalDate() {
        LocalDate result = null;
        Integer intYear = toInteger(this.year);
        Integer intMonth = toInteger(this.month);
        Integer intDay = toInteger(this.day);
        if (!anyNull(intYear, intMonth, intDay)) {
            try {
                result = new LocalDate(intYear, intMonth, intDay);
            } catch (IllegalArgumentException e) {
                LOGGER.debug("toLocalDate() failed for {}", this, e);
            }
        }
        return result;
    }

    public Date toUtilDate() {
        LocalDate localDate = toLocalDate();
        if (localDate != null) {
            return localDate.toDateTimeAtStartOfDay().toDate();
        }
        return null;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    /**
     * toString returns ISO8601 formatted short date.
     */
    @Override
    public String toString() {
        return year + "-" + month + "-" + day;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((day == null) ? 0 : day.hashCode());
        result = prime * result + ((month == null) ? 0 : month.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CompositeDate other = (CompositeDate) obj;
        if (day == null) {
            if (other.day != null)
                return false;
        } else if (!day.equals(other.day))
            return false;
        if (month == null) {
            if (other.month != null)
                return false;
        } else if (!month.equals(other.month))
            return false;
        if (year == null) {
            if (other.year != null)
                return false;
        } else if (!year.equals(other.year))
            return false;
        return true;
    }
}
