package uk.nhs.nhsbsa.time.compositedate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import uk.nhs.nhsbsa.time.util.ObjectUtils;

public class ValidCompositeDateConstraintValidator
        implements ConstraintValidator<ValidDate, CompositeDate> {

    private boolean required;

    @Override
    public void initialize(ValidDate validDate) {
        this.required = validDate.notBlank();
    }

    @Override
    public boolean isValid(CompositeDate value, ConstraintValidatorContext context) {
        boolean valid = true;
        if (requiresValidation(value)) {

            valid = value.toLocalDate() != null;

            // check text entry dd MM yyyy
            valid = valid && fieldLengthValid(value);
        }
        return valid;
    }

    private boolean requiresValidation(CompositeDate value) {
        return value != null && (required
                || !ObjectUtils.allNull(value.getYear(), value.getMonth(), value.getDay()));
    }

    private boolean fieldLengthValid(CompositeDate value) {
        return value.getDay().length() <= 2 && value.getMonth().length() <= 2
                && value.getYear().length() <= 4;
    }
}
