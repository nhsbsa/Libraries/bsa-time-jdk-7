package uk.nhs.nhsbsa.time.util;

public class StringUtils {

    private StringUtils() {}

    public static boolean isBlank(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean anyBlank(String... values) {
        return blank(true, values);
    }

    public static boolean allBlank(String... values) {
        return values.length != 0 && blank(false, values);
    }

    public static boolean blank(boolean matchAny, String... values) {
        for (String value : values) {
            if (matchAny == StringUtils.isBlank(value)) {
                return matchAny;
            }
        }
        return !matchAny;
    }
}
