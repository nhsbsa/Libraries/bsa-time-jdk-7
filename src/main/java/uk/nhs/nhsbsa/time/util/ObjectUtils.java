package uk.nhs.nhsbsa.time.util;

public class ObjectUtils {

    private ObjectUtils() {}

    public static boolean anyNull(Object... values) {
        return isNull(true, values);
    }

    public static boolean allNull(Object... values) {
        return values.length != 0 && isNull(false, values);
    }

    public static boolean isNull(boolean matchAny, Object... values) {
        for (Object value : values) {
            if (matchAny == (value == null)) {
                return matchAny;
            }
        }
        return !matchAny;
    }

}
